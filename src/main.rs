use std::{collections::HashMap, path::{Path, PathBuf}, process::{Child, Command}};

use log::{error, info};
use watchexec::config::{Config, ConfigBuilder};
use anyhow::{Context, anyhow};
use structopt::StructOpt;
use mktemp::Temp;


type Result<T> = anyhow::Result<T>;

#[derive(StructOpt)]
struct Opt {
    /// List of dot files to view
    dotfiles: Vec<PathBuf>,

    /// Name of the desired PDF reader.
    #[structopt(short = "r", default_value = "okular")]
    pdf_reader: String,
}

fn main() -> Result<()> {
    let opts = Opt::from_args();

    env_logger::builder()
        .format_timestamp(None)
        // .filter_module("watchexec", log::LevelFilter::Trace)
        .filter_module("showdot", log::LevelFilter::Info)
        .init();

    let dotfiles = opts.dotfiles
        .into_iter()
        .map(std::fs::canonicalize)
        .collect::<std::io::Result<Vec<PathBuf>>>()
        .context("Failed to cannonicalise paths")?;

    let watcher = Watcher::new(dotfiles, &opts.pdf_reader)?;

    watchexec::watch(&watcher)?;

    Ok(())
}

fn make_pdf(dot: &Path, target: &Path) -> Result<()> {
    let output = Command::new("dot")
        .arg(dot)
        .args(&[
            "-Tpdf",
            "-o"
        ])
        .arg(target)
        .output()?;

    if output.status.success() {
        Ok(())
    }
    else {
        let stdout = String::from_utf8_lossy(&output.stdout);
        let stderr = String::from_utf8_lossy(&output.stderr);
        Err(anyhow!(
            "Failed to build {:?} -> {:?}\nstdout:\n{}\nstderr:\n{}",
            dot,
            target,
            stdout,
            stderr
        ))
    }
}

struct Watcher {
    /// Mapping between dot file and PDF
    pdf_map: HashMap<PathBuf, Temp>,
    _pdf_readers: Vec<Child>
}

impl Watcher {
    fn new(dots: Vec<PathBuf>, pdf_reader: &str) -> Result<Self> {
        let pdf_map = dots.into_iter()
            .map(|dot| {
                let mut dir = PathBuf::new();
                dir.push("/tmp/");
                dir.push("showdot/");
                dir.push(&dot.file_name().context("Dot file had no file name")?);

                std::fs::create_dir_all(&dir)
                    .context(format!("Failed to create dir {:?}", &dir))?;

                let pdf = Temp::new_file_in(&dir)
                    .context("Failed to create temp file")?;

                Ok((dot, pdf))
            })
            .collect::<Result<HashMap<_, _>>>()?;

        // Build PDFs so we have something to open
        for (dot, target) in &pdf_map {
            make_pdf(dot, target)?
        }

        // Open the PDFs
        let pdf_readers = pdf_map.iter()
            .map(|(_, pdf)| {
                Command::new(pdf_reader)
                    .arg(pdf.as_path())
                    .spawn()
                    .context(format!(
                        "Failed to start {} reader for {:?}",
                        pdf_reader,
                        pdf.as_path()
                    ))
            })
            .collect::<Result<Vec<Child>>>()?;

        Ok(Self{pdf_map, _pdf_readers: pdf_readers})
    }
}

impl watchexec::Handler for Watcher {
    fn args(&self) -> Config {
        ConfigBuilder::default()
            .paths(self.pdf_map.keys().cloned().collect::<Vec<_>>())
            .cmd(vec!["date; seq 1 10".into()])
            .build()
            .expect("failed t obuild config")
    }

    fn on_manual(&self) -> watchexec::error::Result<bool> {
        Ok(true)
    }

    fn on_update(&self, ops: &[watchexec::pathop::PathOp]) -> watchexec::error::Result<bool> {
        for op in ops {
            // We'll just assume any event is an update of the file, but we'll break
            // out of the loop to avoid responding to the same underlying event more than
            // once
            info!("Building pdf");
            if let Err(e) = make_pdf(&op.path, &self.pdf_map[&op.path]) {
                error!("Failed to build PDF for {}", e);
            }
            info!("Done");
            break;
        }
        Ok(true)
    }
}
